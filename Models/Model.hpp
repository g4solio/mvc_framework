#ifndef MODEL_H
#define MODEL_H

#include "../Other/Libraries.hpp"
#include "../Other/MVC_Master.hpp"
#include <typeinfo>

class MVC_Master;
using namespace std;
class Model
{

private:

public:

    Model();

    template <class R>
    static R* getObject()
    {
        return static_cast<R*>(MVC_Master::getInstance()->GetModelObject("Test2"));
    }
};



#endif