#include "Controller.hpp"

Controller::Controller()
{
    master = MVC_Master::getInstance();
    master->AddController(this);
}

void Controller::GetNotify(string event, Controller *caller, Arguments* arguments)
{

}

void Controller::Notify(string event, Arguments* arguments)
{
    master->NotifyEvent(event, (Controller *)this, arguments);
}