#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "../Other/Libraries.hpp"
#include "../Other/MVC_Master.hpp"

using namespace std;

class MVC_Master;

class Controller
{
    private:
        MVC_Master *master;
    public:
        Controller();
        void Notify(string event, Arguments* arguments);
        virtual void GetNotify(string event, Controller *caller, Arguments* arguments);
};



#endif