
#ifndef TEST3_H
#define TEST3_H
#include "../Views/View.hpp"

class Test3 : public View
{

    public:
    Test3() {};
    void GetNotify(string event, Controller *caller, Arguments* arguments);

};


#endif